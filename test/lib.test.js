import { describe } from 'riteway'
const debug = require('debug')('test')

import { EventPubSub } from '../index.js'


describe('Pub and Sub', async (assert) => {

  let eventPubSub = EventPubSub()

  let value = null
  let value2 = null
  let key = eventPubSub.subscribe('test',(x)=>{
    value = x
  })
  let key2 = eventPubSub.subscribe('test',(x)=>{
    value2 = x
  })
  eventPubSub.dispatch('test',1)
  eventPubSub.unsubscribe('test',key)
  eventPubSub.unsubscribe('test',key2)
  assert({
    given: "Subscriber and Publish",
    should: "Receive dispatch value",
    actual: `${value} ${value2}`,
    expected: '1 1'
  })
})
