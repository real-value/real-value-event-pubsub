let debug = require('debug')('EventPubSub')


module.exports = {
    EventPubSub: ()=> {
        const events = {}

        let keyCounter = 0
        const dispatch = (event,data) =>{
            if(!events[event]) return
            let subscribers = events[event]
            debug(subscribers)
            Object.keys(subscribers).forEach(key => {
                debug(`dispatch to ${key}`)
                subscribers[key](data)
            })
        }

        const subscribe = (event,callback)=>{
            if(!events[event]) events[event]={}
            keyCounter = keyCounter+1
            let key = `${keyCounter}`
            let subscribers = events[event]
            debug(`Key ${key}`)
            events[event][key]=callback
            debug(Object.keys(subscribers).length)
            return key
        }

        const unsubscribe = (event,key)=>{
            if(!events[event]) return
            delete events[event][key]
        }

        return {
            dispatch,
            subscribe,
            unsubscribe
        }
    }
}

